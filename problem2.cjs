function problem2(inventory){
    if (inventory===undefined){
        return []
    }
    if (Array.isArray(inventory)==false){
        return []
    }
    let x=inventory.length-1
    if (x<0){
        return []
    }
    return inventory[x]
        
}


module.exports=problem2
