function problem3(inventory){
  if (inventory===undefined){
    return []
  }
  if (Array.isArray(inventory)==false){
    
    return []
  }
  inventory.sort((a,b) => (a.car_model > b.car_model) ? 1 : ((b.car_model> a.car_model) ? -1 : 0))
  return inventory
}    


module.exports=problem3
     
